CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Entity Browser Block Layout module extends the Entity Browser Block 
module to provide a better experience when used in the Layout Builder. This 
includes altering the display of the selected blocks to fit in the small 
space available instead of the usual wide table display.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/karens/3023988

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/sandbox/karens/3023988


REQUIREMENTS
------------

- Layout Builder (core)
- Entity Browser (https://www.drupal.org/project/entity_browser)
- Entity Browser Block (https://www.drupal.org/project/entity_browser_block)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and all the
       required modules.
    2. Create an entity browser view of content, following instructions in the 
       Entity Browser module.
    3. Configure a content type to use the Layout Builder, following 
       instructions for Layout Builder.
    4. Optional, edit the content types that are referenced by the entity 
       browser and select the view modes you want to be able to use when adding
       this kind of content to a layout. For instance, you might only want to 
       use the Teaser view mode in that situation.
    5. Create a layout using Layout Builder. In the block list you will see
       block(s) added by Entity Browser Block module for each entity browser
       you created. That will allow you to insert a block into your layout that
       references other content, using entity browser to choose the content.
    6. This module alters the display of the Entity Browser Block when it is 
       used in Layout Builder, and limits the available view modes to the 
       ones selected in option 4 above. 


MAINTAINERS
-----------

Current maintainers:
 * Karen Stevenson (KarenS) - https://www.drupal.org/u/karens

This project is sponsored by:
 * Lullabot - https://www.drupal.org/lullabot


